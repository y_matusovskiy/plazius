﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plazius
{
    public static class RouteConnector
    {
        /// <summary>
        /// Метод для сортировки билетов
        /// </summary>
        /// <param name="cities">Словарь городов</param>
        /// <param name="departures">Массив id городов отправлений (порядок соответствует билетам)</param>
        /// <param name="destinations">Массив id городов назначений (порядок соответствует билетам)</param>
        /// <returns>Массив id билетов в правильном порядке правильный</returns>
        public static int[] ConnectRoutes(Dictionary<int, string> cities, int[] departures, int[] destinations)
        {
            int nCities = cities.Count;
            int nTickets = departures.Length;

            int[] ticketOrder = new int[nTickets]; // искомая последовательность билетов
            int[] map = new int[nCities]; // в каком билете выбранный город является отправлением

            // инициализируем массив
            // если городу соответствует -1, значит из него нет отправлений = конечная точка
            for (var i = 0; i < nCities; i++)
            {
                map[i] = -1;
            }

            for (var i = 0; i < nTickets; i++)
            {
                map[departures[i]] = i;
            }

            int startCity = departures[0]; // начальный город
            ticketOrder[0] = map[startCity]; // находим билет, в котором начальный город является отправлением

            int k = 0;

            while (true)
            {
                var i = ticketOrder[k]; // индекс текущего билета
                var j = destinations[i]; // город назначения для текущего билета

                if (map[j] == -1)
                {
                    break; // нашли конечный город - выходим из цикла
                }

                k++;
                ticketOrder[k] = map[j]; // переходим к следующему билету в цепочке
            }

            return ticketOrder;
        }
    }
}
