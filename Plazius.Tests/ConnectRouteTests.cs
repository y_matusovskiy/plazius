﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Plazius.Tests
{
    [TestClass]
    public class ConnectRouteTests
    {
        [TestMethod]
        public void ConnectRoute_ShouldConnectInRightOrder1()
        {
            // Мельбурн → Кельн
            // Москва → Париж
            // Кельн → Москва

            // Arrange
            var cities = new Dictionary<int, string>
            {
                { 0, "Москва" },
                { 1, "Кельн" },
                { 2, "Париж" },
                { 3, "Мельнбурн" }
            };

            int[] departures = { 3, 0, 1 };
            int[] destinations = { 1, 2, 0 };

            int[] expectedOrder = { 0, 2, 1 };

            // Act
            var ticketOrder = RouteConnector.ConnectRoutes(cities, departures, destinations);

            // Assert
            CollectionAssert.AreEqual(expectedOrder, ticketOrder);
        }

        [TestMethod]
        public void ConnectRoute_ShouldConnectInRightOrder()
        {
            // Берлин → Кельн
            // Пекин → Мельнбурн
            // Москва → Пекин
            // Кельн → Париж
            // Париж → Москва

            // Arrange
            var cities = new Dictionary<int, string>
            {
                { 0, "Москва" },
                { 1, "Кельн" },
                { 2, "Париж" },
                { 3, "Мельнбурн" },
                { 4, "Пекин" },
                { 5, "Берлин" }
            };

            int[] departures = { 5, 4, 0, 1, 2 };
            int[] destinations = { 1, 3, 4, 2, 0 };

            int[] expectedOrder = { 0, 3, 4, 2, 1 };

            // Act
            var ticketOrder = RouteConnector.ConnectRoutes(cities, departures, destinations);

            // Assert
            CollectionAssert.AreEqual(expectedOrder, ticketOrder);
        }
    }
}
